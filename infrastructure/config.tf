variable "region" {
  default = "eu-west-1"
}

variable "stage" {}

provider "aws" {
  region = "${var.region}"
  version = ">= 1.31.0"
}
terraform {
  backend "s3" {
    region = "eu-west-1"
  }
}


