// create an API Gateway
resource "aws_api_gateway_rest_api" "playerApi" {
  name = "${var.name}"
  description = "REST API for ${var.name}"
}

resource "aws_api_gateway_resource" "apiProxyResource" {
  rest_api_id = "${aws_api_gateway_rest_api.playerApi.id}"
  parent_id   = "${aws_api_gateway_rest_api.playerApi.root_resource_id}"
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "apiProxyMethod" {
  rest_api_id                   = "${aws_api_gateway_rest_api.playerApi.id}"
  resource_id                   = "${aws_api_gateway_resource.apiProxyResource.id}"
  http_method                   = "ANY"
  authorization                 = "NONE"
}

resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = "${aws_api_gateway_rest_api.playerApi.id}"
  resource_id = "${aws_api_gateway_method.apiProxyMethod.resource_id}"
  http_method = "${aws_api_gateway_method.apiProxyMethod.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.playerLambda.invoke_arn}"
}

resource "aws_lambda_permission" "apigwToLambda" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.playerLambda.function_name}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.playerApi.execution_arn}/*/*"
}


resource "aws_api_gateway_deployment" "apiDeployment" {
  depends_on = ["aws_api_gateway_method.apiProxyMethod"]
  rest_api_id = "${aws_api_gateway_rest_api.playerApi.id}"
  stage_name = "prod"
  stage_description = "${md5(file("${path.module}/apigateway.tf"))}"
}

//output "api_url" {
//  value = "${dirname(aws_api_gateway_deployment.temp-deployment.invoke_url)}/${aws_api_gateway_stage.api-stage.stage_name}"
//}



output "api_url" {
  value = "${aws_api_gateway_deployment.apiDeployment.invoke_url}"
}

output "api_id" {
  value = "${aws_api_gateway_rest_api.playerApi.id}"
}

