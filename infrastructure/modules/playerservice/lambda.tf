resource "aws_lambda_function" "playerLambda" {
  function_name = "${var.name}"

  filename = "./dist/package.zip"
  source_code_hash = "${base64sha256(file("./dist/package.zip"))}"

  handler = "lambda.handler"
  runtime = "nodejs8.10"

  role = "${aws_iam_role.lambda_role.arn}"

  environment {
    variables = {
      LOG_TABLE_NAME = "${basename(var.logTableArn)}"
      OPPONENTS_STREAM_NAME = "${var.opponentsStreamName}"
      PLAYER_NAME = "${var.name}"
    }
  }
}

resource "aws_iam_role" "lambda_role" {
  name = "role-for-${var.name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_role_policy" {
  name = "lambda_role_author_push-notification"
  role = "${aws_iam_role.lambda_role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
          "Effect": "Allow",
          "Action": [
              "kinesis:DescribeStream",
              "kinesis:GetRecords",
              "kinesis:GetShardIterator",
              "kinesis:ListStreams"
          ],
          "Resource": "${aws_kinesis_stream.playerStream.arn}"
      },
        {
          "Effect": "Allow",
          "Action": [
              "kinesis:PutRecords"
          ],
          "Resource": "arn:aws:kinesis:eu-west-1:523551005302:stream/${var.opponentsStreamName}"
      },
      {
        "Effect": "Allow",
        "Action": [
          "dynamodb:GetItem",
          "dynamodb:Query",
          "dynamodb:Scan",
          "dynamodb:PutItem"
        ],
        "Resource": "${var.logTableArn}"
      }
    ]
}
EOF
}

resource "aws_kinesis_stream" "playerStream" {
  name = "stream-${var.name}"
  shard_count = 1
  retention_period = 24
}

resource "aws_lambda_event_source_mapping" "kinesis_lambda_event_mapping" {
  batch_size = 1
  event_source_arn = "${aws_kinesis_stream.playerStream.arn}"
  enabled = true
  function_name = "${aws_lambda_function.playerLambda.arn}"
  starting_position = "TRIM_HORIZON"
}


resource "aws_cloudwatch_event_rule" "start_play_interval" {
  name = "selfplay_interval-${var.name}"
  description = "selfplay interval"
  schedule_expression = "rate(${var.startToPlayInterval} minutes)"
  is_enabled = true
}

resource "aws_cloudwatch_event_target" "call_patcher_lambda" {
  rule = "${aws_cloudwatch_event_rule.start_play_interval.name}"
  target_id = "patcher"
  arn = "${aws_lambda_function.playerLambda.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_player_lambda" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.playerLambda.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.start_play_interval.arn}"
}

