data "aws_caller_identity" "current" {}

resource "aws_dynamodb_table" "gameof3-log-table" {
  name           = "gameof3-log-table-${var.stage}"
  read_capacity  = 10
  write_capacity = 5
  hash_key       = "GameId"
  range_key      = "Timestamp"

  attribute {
    name = "GameId"
    type = "S"
  }

  attribute {
    name = "Timestamp"
    type = "S"
  }

}

module "playerService1" {
  source = "./modules/playerservice"
  name = "player1-${var.stage}"
  logTableArn = "${aws_dynamodb_table.gameof3-log-table.arn}"
  opponentsStreamName = "stream-player2-${var.stage}"
  startToPlayInterval = "5"
}

module "playerService2" {
  source = "./modules/playerservice"
  name = "player2-${var.stage}"
  logTableArn = "${aws_dynamodb_table.gameof3-log-table.arn}"
  opponentsStreamName = "stream-player1-${var.stage}"
  startToPlayInterval = "8"
}

output "playerservice1_url" {
  value = "${module.playerService1.api_url}/api"
}
output "playerservice2_url" {
  value = "${module.playerService2.api_url}/api"
}
