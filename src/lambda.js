const { handleApiEvent } = require('./handler/api');
const { handleStreamEvent } = require('./handler/stream');
const { handleCloudwatchEvent } = require('./handler/cloudwatch');

const isApigatewayEvent = event => !!event.httpMethod;
const isCloudwatchEvent = event => event.source === 'aws.events';
const isKinesisEvent = event => event.Records && event.Records[0] && event.Records[0].kinesis;

exports.handler = async (event) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  if (isApigatewayEvent(event)) {
    return handleApiEvent(event.httpMethod, event.path, event.body);
  } if (isCloudwatchEvent(event)) {
    return handleCloudwatchEvent(event);
  } if (isKinesisEvent(event)) {
    const { data } = event.Records[0].kinesis;
    const payload = JSON.parse(Buffer.from(data, 'base64').toString('ascii'));
    return handleStreamEvent(payload);
  }
  return console.error('Unknown event - cannot handle and ignore it');
};
