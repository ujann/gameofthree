const uuid = require('uuid');
const { sendMoveToOpponent } = require('../service/moveService');
const { addLogEntry } = require('../service/logService');
const Move = require('../model/move');

module.exports = {
  handleCloudwatchEvent: async () => {
    const startValue = Math.round(Math.random() * 100) + 1;
    const move = new Move(uuid());
    move.makeMove(startValue);
    await addLogEntry(move);
    await sendMoveToOpponent(move);
    console.log('start periodic move to opponent', move);
  },
};
