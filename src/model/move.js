module.exports = class Move {
  constructor(gameId) {
    this.gameId = gameId;
    this.initalValue = -1;
  }

  makeMove(initialValue) {
    this.initalValue = initialValue;
    const remainder = initialValue % 3;
    let response;
    if (remainder === 0) {
      response = initialValue / 3;
    } else if (remainder === 1) {
      response = (initialValue - 1) / 3;
    } else if (remainder === 2) {
      response = (initialValue + 1) / 3;
    }
    this.responseValue = response;
    this.timestamp = new Date().toISOString();
  }

  get initial() {
    return this.initalValue;
  }

  get response() {
    return this.responseValue;
  }

};
