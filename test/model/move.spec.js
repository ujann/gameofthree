/* global describe it */
const { expect } = require('chai');
const Move = require('../../src/model/move');

describe('Move Model', () => {
  [[56, 19], [19, 6], [6, 2], [2, 1]].forEach((elem) => {
    const [start, response] = elem;
    it('should return the right move', () => {
      const move = new Move('id');
      move.makeMove(start);
      expect(move.response).to.equal(response);
    });
  });
});
